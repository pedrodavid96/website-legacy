'use strict';

const http = require('http')
const { deployBranch } = require('./deploy_branch.js')
const fs = require('fs')

/* TODO:
 * - Handle GET of cluster state (to be used by frontend to render cluster state)
 * - Handle DELETE to shut down deployments
 * - Handle authorization (/ authentication)
 */

process.on('SIGINT', () => {
	console.debug('Cleaning up staging area (deployed servers)')
	rmdir('staging')
	console.debug('Exiting...')
	process.exit()
})

function rmdir(d) {
	if (fs.existsSync(d)) {
		fs.readdirSync(d).forEach(function(file) {
			var C = d + '/' + file
			if (fs.statSync(C).isDirectory()) rmdir(C)
			else fs.unlinkSync(C)
		})
		fs.rmdirSync(d)
	}
}

let cluster = 'blue'

function switchCluster() {
	cluster = cluster == 'blue' ? 'green' : 'blue'
	console.debug(`Cluster changed to ${cluster}`)
}

module.exports.switchCluster = switchCluster

const deployedBranches = {};
let childPort = 8081

module.exports.getCluster = () => {
	return {
		cluster: cluster == 'blue' ? 'localhost' : 'localhost',
		port: cluster == 'blue' ? 8081 : 8082
	}
}

module.exports.handleRequest = (reqPath, req, res, logLatency) => {
	const path = reqPath.substring('/management'.length)
	if (req.method == 'POST' && path == '/bg') {
		switchCluster()
		res.write(`Changed to ${cluster} cluster\n`)
		res.end()
		return
	}

	// const tempPath = path.substring(1)
	// const secondBarTemp = tempPath.indexOf('/')
	// const secondBar = secondBarTemp == -1 ? tempPath.length : secondBarTemp
	// const firstPath = tempPath.substring(0, secondBar)
	// const pathToForward = tempPath.substring(secondBar)
	// const segments = path.split('/')
	const [ _empty, _management, branch, ...rest ] = reqPath.split('/')
	const pathToForward = rest.join('/')

	if (req.method == 'POST') {
		deployBranch(branch, childPort, () => {
			console.debug(`Branch ${branch} deployed`)
			deployedBranches[branch] = childPort++
			res.writeHead(302, {
				Location: branch
			})
			res.write(`Deployed branch ${branch} to http://localhost:8080/${_management}/${branch}/`)
			res.end()
			logLatency()
		})
		return
	}
	if (deployedBranches.hasOwnProperty(branch)) {
		const port = deployedBranches[branch]
		const proxyReq = http.request(`http://localhost:${port}/${pathToForward}`, req.options, (proxyRes) => {
			proxyRes.pipe(res)
			proxyRes.on('end', logLatency)
		})
		proxyReq.on('error', (error) => {
			res.writeHead(500, { 'Content-Type': 'text/plain'})
			logLatency()
			switchCluster()
			res.write('website unavailable, please try again later')
			res.end()
		})
		proxyReq.end()
		return
	}
	res.writeHead(404)
	res.write('Not found')
	res.end()
	logLatency()
}
