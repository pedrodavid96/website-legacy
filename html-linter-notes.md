*Note:* This style guide tries to be as strict as possible, given the
current tools.
Although some rules might not be the best choice (and are open to 
discussion), we will favor consistency over using "common standards"
(at least while we don't establish them).

*References:*
https://htmlhint.io/
https://google.github.io/styleguide/htmlcssguide.html#General_Style_Rules

# Enforced

## href absolute or relative
For now we're keeping it as relative, as `file://` is used when opening
the file locally, but `http://` is used when fetching it from the
static server.

## Naming attributes
https://stackoverflow.com/questions/1696864/naming-class-and-id-html-attributes-dashes-vs-underlines

In short:
Use Hyphens to ensure isolation between your HTML and JavaScript.
Hyphens are valid to use in CSS and HTML but not for JavaScript Objects.
A lot of browsers register HTML Ids as global objects on the window/document object.
This might avoid some pains by ensuring they will never conflict with JavaScript.

## HTML Quotation Marks
Use double (`""`) rather than single quotation marks (`''`) around attribute values.
As per Google Style Guides.

# Not enforced yet

## Omit `type` attributes for sheets and scripts
*Unless not CSS and/or JavaScript*
As per Google Style Guides.

## HTML Line-Wrapping
When line-wrapping, each continuation line should be indented at least 4
additional spaces from the original line.

# Not enforced on purpose

## attr-value-not-empty
This would mean boolean variables would have to be at least something like:
```
<div hidden="">
<div hidden="hidden">
```
Which is less concise and even less clearer.
