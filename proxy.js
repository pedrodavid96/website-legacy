'use strict';

const http = require('http')
const url = require('url')
const { performance } = require('perf_hooks')
const management = require('./management.js')

/* TODO:
 * - Avoid logic repetition
 * - Improve server logic, possibly by using a middleware approach
 */

const server = http.createServer((req, res) => {
	const startTime = performance.now()
	const logLatency = () => {
		const latency = (performance.now() - startTime).toFixed(2)
		console.debug(`${latency}ms ${res.statusCode} - Request: ${req.method} ${req.headers.host}${path}`)
	}

	const reqPath = url.parse(req.url).path
	const path = reqPath.endsWith('/') ? `${reqPath}index.html` : reqPath

	if (path.startsWith('/management')) {
		management.handleRequest(path, req, res, logLatency)
	} else {
		const { ip, port } = management.getCluster()
		const proxyReq = http.request(`http://${ip}:${port}${reqPath}`, req.options, (proxyRes) => {
			proxyRes.pipe(res)
			proxyRes.on('end', logLatency)
		})
		proxyReq.on('error', (error) => {
			res.writeHead(500, { 'Content-Type': 'text/plain'})
			management.switchCluster()
			res.write('website unavailable, please try again later')
			res.end()
			logLatency()
		})
		proxyReq.end()
	}
})

server.on('error', (error) => {
	console.log('Error ' + error)
})

server.listen(8080, () => console.log('Server started'))

