'use strict';

const { spawnSync, fork } = require('child_process')

/* TODO: This should call a configurable script, not necessarily the node server right away
 * The server should have an health check for:
 * - waiting for ready
 * - keeping children status up to date
 * It might become tricky to keep the generated processes as children.
 */

module.exports.deployBranch = function (branch, port, callback) {
	const gitClone = spawnSync('git', [ 'clone', 'https://gitlab.com/peddavid/website.git', '-b', `${branch}`, `staging/${branch}` ], { stdio: 'inherit' })
	const childProc = fork('server', [ port ], { stdio: 'inherit', cwd: `staging/${branch}` })
	callback()
}
