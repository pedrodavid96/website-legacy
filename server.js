'use strict';

const http = require('http')
const fs = require('fs')
const url = require('url')
const { performance } = require('perf_hooks')

const server = http.createServer((req, res) => {
	const startTime = performance.now()
	const logLatency = () => {
		const latency = (performance.now() - startTime).toFixed(2)
		console.debug(`${latency}ms ${res.statusCode} - Request: ${req.headers.host}${path}`)
	}

	const reqPath = url.parse(req.url).path
	const path = reqPath.endsWith('/') ? `${reqPath}index.html` : reqPath

	const readStream = fs.createReadStream(`.${path}`)
	readStream.on('end', logLatency)
	readStream.on('error', () => {
		res.statusCode = 404
		res.end()
		logLatency()
	})
	readStream.pipe(res)
})

server.on('error', (error) => {
	console.log('Error ' + error)
})

const port = process.argv[2] || 8081

server.listen(port, () => console.log(`Server started on port ${port}`))

